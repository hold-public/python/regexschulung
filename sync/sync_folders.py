import argparse
from dirsync import sync

parser = argparse.ArgumentParser()
parser.add_argument('--source', type=str, required=True)
parser.add_argument('--dest', type=str, required=True)
args = parser.parse_args()

print(f'Synching from {args.source} to {args.dest}')
sync(sourcedir=args.source, targetdir=args.dest, action='sync', purge=True)
