import importlib
import os
import pathlib
import sys

from definitions import *
from regex_tests import RegexTester, test_my_regexes

# Definitions ###
STUDENTS_TO_OBSERVE = [
]

# Init ###
os.system('clear')
sys.path.append(PATH_CONTAINER_STUDENTS_WORK)
sys.path.append(PATH_CONTAINER_IGNORE_LIST)
from ignore_list import STUDENTS_TO_IGNORE

for student_to_ignore in STUDENTS_TO_IGNORE:
    try:
        STUDENTS.remove(student_to_ignore)
    except ValueError:
        print(f'There is no student {student_to_ignore}!')

# Get ratings ###
ratings = []
problem_students = []

for student in STUDENTS:
    try:
        student_path = pathlib.Path(PATH_CONTAINER_STUDENTS_WORK) / pathlib.Path(student)

        file_monitor_me_not = student_path / FILE_MONITOR_ME_NOT
        if not file_monitor_me_not.exists():
            student_regexes = importlib.import_module(student + '.my_regexes').my_regexes
            rating = test_my_regexes(regexes_to_test=student_regexes, silent=True)
            if rating.GetNrPassedTests() > 0:
                rating.student = student
                ratings.append(rating)
    except:
        problem_students.append(student)
        # raise

# Manual observation ###
if STUDENTS_TO_OBSERVE:
    print(f"Manual observation:")
    for rating_manual in ratings:
        if rating_manual.student in STUDENTS_TO_OBSERVE:
            print(f"Student {rating_manual.student}:")
            rating_manual.PrintOverview(silent=False)

# Syntax error students ###
if problem_students:
    list_expr = "\n"
    print(f"Problem students:\n{list_expr.join(problem_students)}")
    print(f"")

if ratings:
    # Worst students ###
    ratings_worst_first = sorted(ratings, key=lambda rating: rating.GetNrPassedTests(), reverse=False)
    print(f"Support those:")
    for rating_worst_first in ratings_worst_first[:2]:
        print(f"Student {rating_worst_first.student}:")
        rating_worst_first.PrintOverview(silent=False)

    # Task scores ###
    nr_of_tasks = len(RegexTester.test_cases)
    nr_of_students = len(ratings)

    nrs_passed = [0] * nr_of_tasks
    for rating in ratings:
        for task_index, task in enumerate(rating.task_results):
            is_passed = task.IsPassed()
            if is_passed:
                nrs_passed[task_index] += 1

    print(f"Task scores:")
    for task_index, nr_passed in enumerate(nrs_passed):
        task_pc = nr_passed / nr_of_students
        help_str = ' << support here' if task_pc > 0.1 and task_pc < 0.75 else ''
        print(f"Task {task_index:2} was solved by {nr_passed:2} of {nr_of_students:2} people. ({task_pc:.0%}){help_str}")
else:
    print(f"Nobody active yet!")
