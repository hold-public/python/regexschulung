FILE_MONITOR_ME = 'monitor_me'
FILE_MONITOR_ME_NOT = FILE_MONITOR_ME + '_not'

UNATTEMPTED_REGEX = 'Your_Regex_here'
UNATTEMPTED_REPLACEMENT = 'Your_Replacement_here'

TASK_NR_PLACEHOLDER = '__NR__'

FILE_NAME_REGEX = 'regex.py'
FILE_NAME_REGEX_TESTS = 'regex_tests.py'
FILE_MY_REGEXES = 'my_regexes.py'
FILE_TEMPLATE = 'template.py'

STUDENTS = [
    'Adrian Annaheim',
    'Adrian Sallaz',
    'Apollonius Schwarz',
    'Benjamin Wieland',
    'Brigitte Probst',
    'Daniel Flück',
    'Daniel Niggli',
    'David Metzger',
    'Eva Streit',
    'Hugo Ziegler',
    'Jiri Petr',
    'Lorenz Bleuer',
    'Lukas Frei',
    'Marc Sägesser',
    'Markus Veser',
    'Martin Kuhn',
    'Mathieu Bourquin',
    'Matthias Renner',
    'Matthias Witschi',
    'Michael Bernhard',
    'Michael Lüthy',
    'Michael Schuler',
    'Michel Rüegger',
    'Mischa Studer',
    'Nicola Fricker',
    'Nicola Jaggi',
    'Nicolas Andres',
    'Oliver Neuhaus',
    'Pascal Hari',
    'Patrick Haldi',
    'Patrick Heiniger',
    'Roland Schenk',
    'Thomas Scheidegger',
    'Urs Kofmel',
    'Urs Ryf',
]

# Paths
