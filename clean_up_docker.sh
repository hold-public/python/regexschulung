running_containers=$(docker ps -q)

if [[ -n "$running_containers" ]]; then
  echo "Killed containers:"
  docker kill $running_containers
fi
