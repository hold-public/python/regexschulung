import re
from dataclasses import dataclass, field

@dataclass
class TestCase:
    subject_string: str
    expected_matches: list
    expected_repl: str
    hint: str


@dataclass
class TestResults:
    passed: bool = False

    def IsPassed(self):
        return self.passed


@dataclass
class TaskResults:
    attempted: bool = False
    regex = None
    test_results: list = field(default_factory=list)

    def GetNrTests(self):
        return len(self.test_results)

    def GetNrPassedTests(self):
        nr_passed = 0
        for test_results in self.test_results:
            if test_results.IsPassed():
                nr_passed += 1
        return nr_passed

    def IsPassed(self):
        return self.GetNrTests() == self.GetNrPassedTests()

    def PrintResults(self, silent):
        if self.attempted:
            nr_passed = self.GetNrPassedTests()
            nr_tests = self.GetNrTests()
            passed = nr_passed == nr_tests
            if not passed:
                search_str = f'{self.regex.search_expr}'
                if self.regex.replace_expr is not None:
                    replace_str = f' --> {self.regex.replace_expr}'
                else:
                    replace_str = f''
                regex_str = f' Faulty Regex: {search_str}{replace_str}'
            else:
                regex_str = ''
            print_if_allowed(f'{nr_passed:2} / {nr_tests:2} tests passed. ({nr_passed / nr_tests:4.0%}){regex_str}', silent)
        else:
            print_if_allowed(f'not yet attempted', silent)


@dataclass
class Rating:
    student: str = None
    task_results: list = field(default_factory=list)

    def GetNrTasks(self):
        return len(self.task_results)

    def GetNrPassedTasks(self):
        nr_passed = 0
        for task_results in self.task_results:
            if task_results.IsPassed():
                nr_passed += 1
        return nr_passed

    def GetNrTests(self):
        nr_tests = 0
        for task_results in self.task_results:
            nr_tests += task_results.GetNrTests()
        return nr_tests

    def GetNrPassedTests(self):
        nr_passed_tests = 0
        for task_results in self.task_results:
            nr_passed_tests += task_results.GetNrPassedTests()
        return nr_passed_tests

    def IsPassed(self):
        return self.GetNrTasks() == self.GetNrPassedTasks()

    def PrintOverview(self, silent):
        print_if_allowed(f'Rating overview:', silent)
        for task_index, task in enumerate(self.task_results):
            print_without_newline_if_allowed(f'Task {task_index:2}: ', silent)
            task.PrintResults(silent)
        print_if_allowed(f'', silent)
        nr_passed = self.GetNrPassedTests()
        nr_tests = self.GetNrTests()
        if nr_tests:
            score = nr_passed / nr_tests
        else:
            score = 0
        print_if_allowed(f'Overall score: {score:.0%}', silent)
        print_if_allowed(f'', silent)


class RegexTester():
    test_cases = [
        [ # Task "yay in text"
            TestCase(subject_string='yay', expected_matches=['yay'], expected_repl=None, hint='You shall match "yay"!'),
            TestCase(subject_string='yay yay', expected_matches=['yay', 'yay'], expected_repl=None, hint='"yay" can also appear more than once!'),
            TestCase(subject_string='yayeee', expected_matches=['yay'], expected_repl=None, hint='"yay" can also be a part of a word!'),
            TestCase(subject_string='ayay', expected_matches=['yay'], expected_repl=None, hint='"yay" can also be a part of a word!'),
            TestCase(subject_string='yey', expected_matches=[], expected_repl=None, hint='"yey" is not "yay"!'),
            TestCase(subject_string='grey', expected_matches=[], expected_repl=None, hint='"grey" is not "yay"'),
        ],
        [ # Task "only yay"
            TestCase(subject_string='yay', expected_matches=['yay'], expected_repl=None, hint='It the subject text is exactly "yay", it should match!'),
            TestCase(subject_string='yay yay', expected_matches=[], expected_repl=None, hint='This time, your Regex should only match if the subject string is _only_ "yay"!'),
            TestCase(subject_string='yayeee', expected_matches=[], expected_repl=None, hint='This time, your Regex should only match if the subject string is _only_ "yay"!'),
            TestCase(subject_string='ayay', expected_matches=[], expected_repl=None, hint='This time, your Regex should only match if the subject string is _only_ "yay"!'),
            TestCase(subject_string='yey', expected_matches=[], expected_repl=None, hint='yey is not yay'),
            TestCase(subject_string='grey', expected_matches=[], expected_repl=None, hint='grey is not yay'),
        ],
        [ # Task "AE vs. BE"
            TestCase(subject_string='gray', expected_matches=['gray'], expected_repl=None, hint='Please match the word "gray"'),
            TestCase(subject_string='grey', expected_matches=['grey'], expected_repl=None, hint='Please match the word "grey"'),
            TestCase(subject_string='greay', expected_matches=[], expected_repl=None, hint='Only one of the letters "e" or "a", not both!'),
            TestCase(subject_string='graey', expected_matches=[], expected_repl=None, hint='Only one of the letters "a" or "e", not both!'),
            TestCase(subject_string='graybeard', expected_matches=[], expected_repl=None, hint='This time, we only want full matches, not finding "gray" or "grey" in a larger word or text!'),
            TestCase(subject_string='greybeard', expected_matches=[], expected_repl=None, hint='This time, we only want full matches, not finding "gray" or "grey" in a larger word or text!'),
            TestCase(subject_string='stonegray', expected_matches=[], expected_repl=None, hint='This time, we only want full matches, not finding "gray" or "grey" in a larger word or text!'),
            TestCase(subject_string='stonegrey', expected_matches=[], expected_repl=None, hint='This time, we only want full matches, not finding "gray" or "grey" in a larger word or text!'),
        ],
        [ # Task "dada"
            TestCase(subject_string='gray', expected_matches=['gray'], expected_repl=None, hint='Come on, at least gray should match!'),
            TestCase(subject_string='grby', expected_matches=['grby'], expected_repl=None, hint='Any symbol must be permitted as the third character!'),
            TestCase(subject_string='grcy', expected_matches=['grcy'], expected_repl=None, hint='Any symbol must be permitted as the third character!'),
            TestCase(subject_string='grzy', expected_matches=['grzy'], expected_repl=None, hint='Any symbol must be permitted as the third character!'),
            TestCase(subject_string='gr0y', expected_matches=['gr0y'], expected_repl=None, hint='Any symbol must be permitted as the third character!'),
            TestCase(subject_string='gr9y', expected_matches=['gr9y'], expected_repl=None, hint='Any symbol must be permitted as the third character!'),
            TestCase(subject_string='gr&y', expected_matches=['gr&y'], expected_repl=None, hint='Any symbol must be permitted as the third character!'),
            TestCase(subject_string='gr_y', expected_matches=['gr_y'], expected_repl=None, hint='Any symbol must be permitted as the third character!'),
            TestCase(subject_string='gry', expected_matches=[], expected_repl=None, hint='There must be a character between "gr" and "y", it cannot be omitted!'),
            TestCase(subject_string='graay', expected_matches=[], expected_repl=None, hint='Only one character between "gr" and "y" please!'),
            TestCase(subject_string='grayhound', expected_matches=[], expected_repl=None, hint='Make sure that the subject text is matched in its entirety!'),
            TestCase(subject_string='gr\ny', expected_matches=[], expected_repl=None, hint='We don\'t want to match a newline!'),
        ],
        [ # Task "keywords"
            TestCase(subject_string='', expected_matches=[], expected_repl=None, hint='Don\'t match the empty string! (Did you use two ||?)'),
            TestCase(subject_string='if', expected_matches=['if'], expected_repl=None, hint='The word "if" is on the whitelist!'),
            TestCase(subject_string='else', expected_matches=['else'], expected_repl=None, hint='The word "else" is on the whitelist!'),
            TestCase(subject_string='while', expected_matches=['while'], expected_repl=None, hint='The word "while" is on the whitelist!'),
            TestCase(subject_string='do', expected_matches=['do'], expected_repl=None, hint='The word "do" is on the whitelist!'),
            TestCase(subject_string='for', expected_matches=['for'], expected_repl=None, hint='The word "for" is on the whitelist!'),
            TestCase(subject_string='if else while do for', expected_matches=['if', 'else', 'while', 'do', 'for'], expected_repl=None, hint='Please also permit more than one word!'),
            TestCase(subject_string='fork', expected_matches=['for'], expected_repl=None, hint='The word "or" can also be part of a bigger word!'),
            TestCase(subject_string='during', expected_matches=[], expected_repl=None, hint='during is not a keyword!'),
        ],
        [ # Task "80 or 443"
            TestCase(subject_string='http', expected_matches=['http'], expected_repl=None, hint='"http" is one of the permitted words!'),
            TestCase(subject_string='https', expected_matches=['https'], expected_repl=None, hint='"https" is one of the permitted words!'),
            TestCase(subject_string='httpl', expected_matches=[], expected_repl=None, hint='httpl is not one of the allowed words!'),
            TestCase(subject_string='htt', expected_matches=[], expected_repl=None, hint='missing p'),
            TestCase(subject_string='httpse', expected_matches=[], expected_repl=None, hint='extra e'),
            TestCase(subject_string='ahttp', expected_matches=[], expected_repl=None, hint='Match only the whole words "http" or "https"!'),
        ],
        [ # Task "10000"
            TestCase(subject_string='1', expected_matches=['1'], expected_repl=None, hint='No 0s must also be allowed!'),
            TestCase(subject_string='10', expected_matches=['10'], expected_repl=None, hint='One 0 must also be allowed!'),
            TestCase(subject_string='10000', expected_matches=['10000'], expected_repl=None, hint='Why don\'t you like this perfectly valid number? *sad*'),
            TestCase(subject_string='20', expected_matches=[], expected_repl=None, hint='Only _1_ followed by zeros, not other digits!'),
            TestCase(subject_string='10020', expected_matches=[], expected_repl=None, hint='No other digits than 1 followed by 0s!'),
            TestCase(subject_string=' 10000', expected_matches=[], expected_repl=None, hint='The subject string must contain nothing more than the asked number!'),
            TestCase(subject_string='10000a', expected_matches=[], expected_repl=None, hint='The subject string must contain nothing more than the asked number!'),
        ],
        [ # Task "wohooo"
            TestCase(subject_string='woho', expected_matches=['woho'], expected_repl=None, hint='One "o" must be accepted!'),
            TestCase(subject_string='wohooo', expected_matches=['wohooo'], expected_repl=None, hint='Three "o"s must be accepted!'),
            TestCase(subject_string='wohooooooooooooooooooooooo', expected_matches=['wohooooooooooooooooooooooo'], expected_repl=None, hint='Many "o"s must be ok!'),
            TestCase(subject_string='woh', expected_matches=[], expected_repl=None, hint='If no "o" follows "woh", it should not match!'),
            TestCase(subject_string='wohaoooo', expected_matches=[], expected_repl=None, hint='Why is this extra "a" matched??'),
            TestCase(subject_string=' woho', expected_matches=[], expected_repl=None, hint='The whole subject string must be matched, not just a part of it!'),
            TestCase(subject_string='wohooooa', expected_matches=[], expected_repl=None, hint='Extra "a" at the end!'),
        ],
        [ # Task "space, the final frontier"
            TestCase(subject_string='foo  bar', expected_matches=['foo  bar'], expected_repl=None, hint='Two words with several spaces in between must be matched!'),
            TestCase(subject_string='foo   bar', expected_matches=['foo   bar'], expected_repl=None, hint='Two words with several spaces in between must be matched!'),
            TestCase(subject_string='foo    bar', expected_matches=['foo    bar'], expected_repl=None, hint='Two words with several spaces in between must be matched!'),
            TestCase(subject_string='foo bar  zah', expected_matches=['bar  zah'], expected_repl=None, hint='You don\'t have to match the whole subject string this time!'),
            TestCase(subject_string='foo  bar bar  zah', expected_matches=['foo  bar', 'bar  zah'], expected_repl=None, hint='You don\'t have to match the whole subject string this time!'),
            TestCase(subject_string='foo bar zah', expected_matches=[], expected_repl=None, hint='There are two spaces, but they are not consecutive!'),
            TestCase(subject_string='foo bar', expected_matches=[], expected_repl=None, hint='The two words are separated only by one space!'),
            TestCase(subject_string='  foo', expected_matches=[], expected_repl=None, hint='There are two spaces, but they are not between two words!'),
            TestCase(subject_string='foo  ', expected_matches=[], expected_repl=None, hint='There are two spaces, but they are not between two words!'),
            TestCase(subject_string='foo  123', expected_matches=[], expected_repl=None, hint='The second thing is not a word. Look at those disgusting numbers!'),
        ],
        [ # Task "no vowels"
            TestCase(subject_string='klngn', expected_matches=['klngn'], expected_repl=None, hint='Worf is sad!'),
            TestCase(subject_string='rgx', expected_matches=['rgx'], expected_repl=None, hint='who needs "e"s??'),
            TestCase(subject_string='rs232', expected_matches=['rs232'], expected_repl=None, hint='Numbers are not vowels!'),
            TestCase(subject_string='ç%&"*', expected_matches=['ç%&"*'], expected_repl=None, hint='We like special characters!'),
            TestCase(subject_string='axy', expected_matches=[], expected_repl=None, hint='"a" is a dirty vowel!'),
            TestCase(subject_string='CSA', expected_matches=[], expected_repl=None, hint='"A" is a dirty vowel, and a big one!'),
            TestCase(subject_string='x y', expected_matches=[], expected_repl=None, hint='We also don\'t like spaces!'),
        ],
        [ # Task "stray {"
            TestCase(subject_string='{', expected_matches=['{'], expected_repl=None, hint='A line with just an opening brace { should be matched!'),
            TestCase(subject_string='{ ', expected_matches=['{ '], expected_repl=None, hint='Allow trailing whitespace!'),
            TestCase(subject_string='{  ', expected_matches=['{  '], expected_repl=None, hint='Allow any amount of trailing whitespace!'),
            TestCase(subject_string=' {', expected_matches=[' {'], expected_repl=None, hint='Allow leading whitespace!'),
            TestCase(subject_string='    {', expected_matches=['    {'], expected_repl=None, hint='Allow any amount of leading whitespace!'),
            TestCase(subject_string=' { ', expected_matches=[' { '], expected_repl=None, hint='Allow leading and trailing whitespace!'),
            TestCase(subject_string='   {   ', expected_matches=['   {   '], expected_repl=None, hint='Allow any amount of leading and trailing whitespace!'),
            TestCase(subject_string='while {', expected_matches=[], expected_repl=None, hint='{ is on same line as while, this should not be matched!'),
            TestCase(subject_string='{ extra', expected_matches=[], expected_repl=None, hint='There is some non-whitespace after the opening brace {!'),
        ],
        [ # Task "079 het sie gseit"
            TestCase(subject_string='079 123 45 67', expected_matches=['079 123 45 67'], expected_repl=None, hint='Match a perfectly normal phone number!'),
            TestCase(subject_string='079 956 11 32', expected_matches=['079 956 11 32'], expected_repl=None, hint='Match a perfectly normal phone number!'),
            TestCase(subject_string='079 000 00 00', expected_matches=['079 000 00 00'], expected_repl=None, hint='This is the lowest allowed phone number!'),
            TestCase(subject_string='079 999 99 99', expected_matches=['079 999 99 99'], expected_repl=None, hint='This is the highest allowed phone number!'),
            TestCase(subject_string='078 123 45 67', expected_matches=[], expected_repl=None, hint='078 is not 079'),
            TestCase(subject_string='031 123 45 67', expected_matches=[], expected_repl=None, hint='031 is not 079'),
            TestCase(subject_string='079 999 99 999', expected_matches=[], expected_repl=None, hint='The last digit group contains a trailing number. Did you match the whole subject string?'),
            TestCase(subject_string='079 999 99 99a', expected_matches=[], expected_repl=None, hint='The last digit group contains a trailing letter. Did you match the whole subject string?'),
            TestCase(subject_string='0079 999 99 99', expected_matches=[], expected_repl=None, hint='The first digit group contains a leading extra number. Did you match the whole subject string?'),
            TestCase(subject_string='a079 999 99 99', expected_matches=[], expected_repl=None, hint='The first digit group contains a leading extra letter. Did you match the whole subject string?'),
        ],
        [ # Task "variable name"
            TestCase(subject_string='abc123_', expected_matches=['abc123_'], expected_repl=None, hint='Letters, numbers and _ are all allowed!'),
            TestCase(subject_string='abc_123', expected_matches=['abc_123'], expected_repl=None, hint='Letters, numbers and _ are all allowed!'),
            TestCase(subject_string='123abc_', expected_matches=['123abc_'], expected_repl=None, hint='Letters, numbers and _ are all allowed!'),
            TestCase(subject_string='_abc123', expected_matches=['_abc123'], expected_repl=None, hint='Letters, numbers and _ are all allowed!'),
            TestCase(subject_string='_123abc', expected_matches=['_123abc'], expected_repl=None, hint='Letters, numbers and _ are all allowed!'),
            TestCase(subject_string='a', expected_matches=['a'], expected_repl=None, hint='Short variable names are also allowed!'),
            TestCase(subject_string='1', expected_matches=['1'], expected_repl=None, hint='Short variable names are also allowed!'),
            TestCase(subject_string='_', expected_matches=['_'], expected_repl=None, hint='Short variable names are also allowed!'),
            TestCase(subject_string='', expected_matches=[], expected_repl=None, hint='The variable needs to have at least one character!'),
            TestCase(subject_string='a?bc', expected_matches=[], expected_repl=None, hint='Other special characters are not allowed!'),
            TestCase(subject_string='a.bc', expected_matches=[], expected_repl=None, hint='Other special characters are not allowed!'),
            TestCase(subject_string='a-bc', expected_matches=[], expected_repl=None, hint='Other special characters are not allowed!'),
            TestCase(subject_string='a bc', expected_matches=[], expected_repl=None, hint='Other special characters are not allowed!'),
            TestCase(subject_string=' abc', expected_matches=[], expected_repl=None, hint='Be sure to match the whole subject text!'),
            TestCase(subject_string='abc ', expected_matches=[], expected_repl=None, hint='Be sure to match the whole subject text!'),
        ],
        [ # Task "whole yay"
            TestCase(subject_string='yay', expected_matches=['yay'], expected_repl=None, hint='It the subject text is exactly "yay", it should match!'),
            TestCase(subject_string=' yay ', expected_matches=['yay'], expected_repl=None, hint='It\'s not as simple as adding spaces, what if yay is the first or last word?'),
            TestCase(subject_string='yay foo', expected_matches=['yay'], expected_repl=None, hint='It\'s ok that there is another word, no need to use ^ and $'),
            TestCase(subject_string='yay foo yay', expected_matches=['yay', 'yay'], expected_repl=None, hint='Be sure to find all "yay"s in the subject text!'),
            TestCase(subject_string='foo yay', expected_matches=['yay'], expected_repl=None, hint='It\'s ok that there is another word, no need to use ^ and $'),
            TestCase(subject_string='yayeee', expected_matches=[], expected_repl=None, hint='We don\'t want words that only contain "yay"!'),
            TestCase(subject_string='ayay', expected_matches=[], expected_repl=None, hint='We don\'t want words that only contain "yay"!'),
        ],
        [ # Task "E-Mail"
            TestCase(subject_string='foo@bar.com', expected_matches=['foo@bar.com'], expected_repl=None, hint='This is a perfectly valid E-Mail!'),
            TestCase(subject_string='f@b.ch', expected_matches=['f@b.ch'], expected_repl=None, hint='I know it\'s short, but according to our definition, it\'s allowed!'),
            TestCase(subject_string='foo123@bar.ch', expected_matches=['foo123@bar.ch'], expected_repl=None, hint='Numbers must be allowed too!'),
            TestCase(subject_string='foo@bar.tattoo', expected_matches=['foo@bar.tattoo'], expected_repl=None, hint='Longer top level domains must also be allowed!'),
            TestCase(subject_string='foo@barcom', expected_matches=[], expected_repl=None, hint='There is no "."! Did you forget to escape the "."?'),
            TestCase(subject_string='foo@bar.co_m', expected_matches=[], expected_repl=None, hint='_ is not allowed in a top level domain!'),
            TestCase(subject_string='foo@bar.toolong', expected_matches=[], expected_repl=None, hint='Top level domain is too long!'),
            TestCase(subject_string='foo@bar.s', expected_matches=[], expected_repl=None, hint='Top level domain is too short!'),
            TestCase(subject_string='foo foo@bar.ch', expected_matches=[], expected_repl=None, hint='No spaces!'),
            TestCase(subject_string='text_before foo@bar.ch', expected_matches=[], expected_repl=None, hint='No extra text before E-Mail!'),
            TestCase(subject_string='foo@bar.ch text_after', expected_matches=[], expected_repl=None, hint='No extra text after E-Mail!'),
        ],
        [ # Task "duplicate words"
            TestCase(subject_string='foo foo', expected_matches=['foo foo'], expected_repl=None, hint='Duplicate words "foo" should be matched!'),
            TestCase(subject_string='bar bar', expected_matches=['bar bar'], expected_repl=None, hint='Duplicate words "bar" should be matched!'),
            TestCase(subject_string='foo  foo', expected_matches=['foo  foo'], expected_repl=None, hint='Also allow more than one whitespace between duplicate words'),
            TestCase(subject_string='foo\tfoo', expected_matches=['foo\tfoo'], expected_repl=None, hint='Also allow tabs between duplicate words'),
            TestCase(subject_string='text before foo foo', expected_matches=['foo foo'], expected_repl=None, hint='There can be extra text before the duplicate words!'),
            TestCase(subject_string='foo foo text after', expected_matches=['foo foo'], expected_repl=None, hint='There can be extra text after the duplicate words!'),
            TestCase(subject_string='foo bar foo', expected_matches=[], expected_repl=None, hint='Only _consecutive_ duplicate words should be matched!'),
            TestCase(subject_string='foo bar', expected_matches=[], expected_repl=None, hint='It has to be twice the same word!'),
            TestCase(subject_string='foo fool', expected_matches=[], expected_repl=None, hint='Be careful to match only duplicate _whole_ words!'),
            TestCase(subject_string='afoo foo', expected_matches=[], expected_repl=None, hint='Be careful to match only duplicate _whole_ words!'),
        ],
        [ # Task "Google style guide"
            TestCase(subject_string='while (true){', expected_matches=['while (true){'], expected_repl='while (true) {', hint='There is no space between (true) and {'),
            TestCase(subject_string='while (false){', expected_matches=['while (false){'], expected_repl='while (false) {', hint='There is no space between (false) and {'),
            TestCase(subject_string='while (true)', expected_matches=[], expected_repl=None, hint='There is no { at all!'),
            TestCase(subject_string='while (false){extra', expected_matches=[], expected_repl=None, hint='No extra text after the { is allowed!'),
            TestCase(subject_string='(true){', expected_matches=[], expected_repl=None, hint='The keyword is missing!'),
            TestCase(subject_string='while (true) {', expected_matches=[], expected_repl=None, hint='This line is already perfectly conform to google style guide!'),
            TestCase(subject_string='while true){', expected_matches=[], expected_repl=None, hint='We need an opening (!'),
            TestCase(subject_string='while\t(true){', expected_matches=[], expected_repl=None, hint='Tabs are not valid separators!'),
        ],
        [ # Task "first names"
            TestCase(subject_string='Adrian Berger', expected_matches=['Adrian'], expected_repl=None, hint='Match only the first name!'),
            TestCase(subject_string='Adrian Berger, Ben Zuger', expected_matches=['Adrian'], expected_repl=None, hint='Match only people with the last name "Berger"!'),
            TestCase(subject_string='Adrian Bergerson', expected_matches=[], expected_repl=None, hint='Match only people with the last name "Berger", not names that merely start with "Berger"!'),
            TestCase(subject_string='Adrian Berger, Ben Zuger, Charlie Berger, Dominik Schaffner, Erik Berger', expected_matches=['Adrian', 'Charlie', 'Erik'], expected_repl=None, hint='Match only people with the last name "Berger", not names that merely start with "Berger"!'),
            TestCase(subject_string='1234 Berger', expected_matches=[], expected_repl=None, hint='Names usually don\'t consist of numbers!'),
            TestCase(subject_string=' Berger', expected_matches=[], expected_repl=None, hint='Names cannot be empty!'),
        ],
        [ # Task "q but not qu"
            TestCase(subject_string='q', expected_matches=['q'], expected_repl=None, hint='A simple "q" should be matched!'),
            TestCase(subject_string='qa', expected_matches=['qa'], expected_repl=None, hint='"a" is not "u", so this should be matched!'),
            TestCase(subject_string='qau', expected_matches=['qau'], expected_repl=None, hint='It\'t ok, "q" and "u" brought some distance between them!'),
            TestCase(subject_string='aqa', expected_matches=[], expected_repl=None, hint='Only match words that start with "q"!'),
            TestCase(subject_string='qu', expected_matches=[], expected_repl=None, hint='"q" is followed by "u", so this must not be matched!'),
        ],
        [ # Task "Michael's favourite"
            TestCase(subject_string='<a>', expected_matches=['<a>'], expected_repl=None, hint='Make sure to also match the < >'),
            TestCase(subject_string='<1>', expected_matches=['<1>'], expected_repl=None, hint='Don\'t match only letters, also numbers!'),
            TestCase(subject_string='<@>', expected_matches=['<@>'], expected_repl=None, hint='Don\'t match only letters, also symbols!'),
            TestCase(subject_string='<>', expected_matches=['<>'], expected_repl=None, hint='Empty <> are also ok!'),
            TestCase(subject_string='Houston, we have a problem with <string one> and <string two>. Please respond.', expected_matches=['<string one>', '<string two>'], expected_repl=None, hint='Be lazy'),
        ],
        [ # Task "bad cat"
            TestCase(subject_string='car', expected_matches=['car'], expected_repl=None, hint='A car is not a cat!'),
            TestCase(subject_string='rat', expected_matches=['rat'], expected_repl=None, hint='I smell a rat!'),
            TestCase(subject_string='catering', expected_matches=['catering'], expected_repl=None, hint='Only the whole word "cat" shall be avoided, not words containing it!'),
            TestCase(subject_string='netcat', expected_matches=['netcat'], expected_repl=None, hint='Only the whole word "cat" shall be avoided, not words containing it!'),
            TestCase(subject_string='tac', expected_matches=['tac'], expected_repl=None, hint='You can\'t just avoid all the letters in the word "cat"!'),
            TestCase(subject_string='', expected_matches=[], expected_repl=None, hint='Don\'t match empty words!'),
            TestCase(subject_string='cat', expected_matches=[], expected_repl=None, hint='Cat got your Regex, Mr Bond?'),
        ],
        [ # Task "octals"
            TestCase(subject_string='0123', expected_matches=['0123'], expected_repl=None, hint='This is a normal octal number, it should be matched!'),
            TestCase(subject_string='01', expected_matches=['01'], expected_repl=None, hint='This is a normal octal number, it should be matched!'),
            TestCase(subject_string='007', expected_matches=['007'], expected_repl=None, hint='Octals can have more than one leading zero! (https://en.wikipedia.org/wiki/Leading_zero)'),
            TestCase(subject_string='01 12 07 009 023', expected_matches=['01', '07', '023'], expected_repl=None, hint='Some of these are normal octals, they should be matched!'),
            TestCase(subject_string='08', expected_matches=[], expected_repl=None, hint='Octals can only have numbers from 0 to 7!'),
            TestCase(subject_string='079', expected_matches=[], expected_repl=None, hint='Octals can only have numbers from 0 to 7!'),
            TestCase(subject_string='0', expected_matches=[], expected_repl=None, hint='This could also be a decimal zero, do not match it!'),
            TestCase(subject_string='123', expected_matches=[], expected_repl=None, hint='This is just a decimal number, no matching please!'),
            TestCase(subject_string='0xyz', expected_matches=[], expected_repl=None, hint='This is not even a number!'),
        ],
        [ # Task "annoying pw checker"
            TestCase(subject_string='a1cDef_h', expected_matches=['a1cDef_h'], expected_repl=None, hint='This password fulfills all requirements!'),
            TestCase(subject_string='a12Def_h', expected_matches=['a12Def_h'], expected_repl=None, hint='This password fulfills all requirements!'),
            TestCase(subject_string='a1cDEF_h', expected_matches=['a1cDEF_h'], expected_repl=None, hint='This password fulfills all requirements!'),
            TestCase(subject_string='a1cD{&_h', expected_matches=['a1cD{&_h'], expected_repl=None, hint='This password fulfills all requirements!'),
            TestCase(subject_string='sH1$', expected_matches=[], expected_repl=None, hint='This password is too short!'),
            TestCase(subject_string='a1cDef_abcdefghijklmnopqrstuvwxyz', expected_matches=[], expected_repl=None, hint='This password is too long!'),
            TestCase(subject_string='A1CDEF_H', expected_matches=[], expected_repl=None, hint='This password has no lower case letters in it!'),
            TestCase(subject_string='a1cdef_h', expected_matches=[], expected_repl=None, hint='This password has no upper case letters in it!'),
            TestCase(subject_string='abcDef_h', expected_matches=[], expected_repl=None, hint='This password has no digits in it!'),
            TestCase(subject_string='a1cDefgh', expected_matches=[], expected_repl=None, hint='This password has no special characters in it!'),
        ],
        [ # Task "separators"
            TestCase(subject_string="1'000", expected_matches=["1'000"], expected_repl=None, hint='This number should be matched!'),
            TestCase(subject_string="1'000'000", expected_matches=["1'000'000"], expected_repl=None, hint='This number should be matched!'),
            TestCase(subject_string="100", expected_matches=["100"], expected_repl=None, hint='Although it doesn\'t have an apostrophe due to its shortness, this is still a legit number!'),
            TestCase(subject_string="1'234'567'890", expected_matches=["1'234'567'890"], expected_repl=None, hint='Support all numbers 0-9!'),
            TestCase(subject_string="12'345'678", expected_matches=["12'345'678"], expected_repl=None, hint='Support all numbers 0-9!'),
            TestCase(subject_string="123'456'789", expected_matches=["123'456'789"], expected_repl=None, hint='Support all numbers 0-9!'),
            TestCase(subject_string="0", expected_matches=["0"], expected_repl=None, hint='Zero is also a valid number!'),
            TestCase(subject_string="01", expected_matches=[], expected_repl=None, hint='This looks like a dirty octal number!'),
            TestCase(subject_string="1'000'0", expected_matches=[], expected_repl=None, hint='The apostrophe is on the wrong spot!'),
            TestCase(subject_string="1000", expected_matches=[], expected_repl=None, hint='There is no apostrophe where there should be one!'),
            TestCase(subject_string="1000'", expected_matches=[], expected_repl=None, hint='There is a trailing apostrophe!'),
            TestCase(subject_string="'000", expected_matches=[], expected_repl=None, hint='There is an extra leading apostrophe!'),
            TestCase(subject_string="0'000'", expected_matches=[], expected_repl=None, hint='This number is all zeros!'),
        ],
    ]

    def list_matches(self, match_list):
        if match_list:
            matches = '\nand\n  '.join(match_list)
        else:
            matches = 'nothing'
        return matches

    def eval_regex(self, regex_to_test, test_case, silent):
        search_expr = regex_to_test.search_expr
        replace_expr = regex_to_test.replace_expr

        subject_string = test_case.subject_string
        expected_matches = test_case.expected_matches
        expected_repl = test_case.expected_repl

        if not expected_matches and expected_repl is not None:
            raise ValueError('wrong test case config!')
        try:
            real_matches = []
            for match in re.finditer(pattern=search_expr, string=subject_string):
                real_matches.append(match.group(0))
        except re.error as e:
            print_if_allowed(f"Your Regex has a syntax error! ({e})!", silent)
            real_matches = []
        does_match = len(real_matches) > 0
        regex_ok = True
        if expected_matches != real_matches:
            print_if_allowed(f"For the subject string\n"
            f"  {subject_string}\n"
            f"we expected to match\n"
            f"  {self.list_matches(expected_matches)}\n"
            f"but matched\n"
            f"  {self.list_matches(real_matches)}\n"
            ,
            silent
            )
            regex_ok = False

        if does_match:
            if expected_repl is not None:
                if replace_expr is not None:
                    try:
                        replacement = re.sub(pattern=search_expr, repl=replace_expr, string=subject_string)
                    except re.error as e:
                        print_if_allowed(f"Your Regex has a syntax error! ({e})!", silent)
                        replacement = ''
                    replace_ok = replacement == expected_repl
                    if not replace_ok:
                        print_if_allowed(f"Your Regex replacement\n"
                        f"  {search_expr} -> {replace_expr}\n"
                        f"replaced\n"
                        f"  {subject_string}\n"
                        f"by\n"
                        f"  {replacement}\n"
                        f"but should have replaced it by \n"
                        f"  {expected_repl}\n",
                        silent
                        )
                        regex_ok = False
                else:
                    print_if_allowed(f"You need to provide a replacement string to replace\n"
                        f"  {subject_string}\n"
                        f"by\n"
                        f"  {expected_repl}\n",
                        silent)
                    regex_ok = False

        if not regex_ok:
            print_if_allowed(f'Hint: {test_case.hint}', silent)
            print_if_allowed('-' * 30, silent)
            print_if_allowed('', silent)

        return regex_ok

    def test_regexes(self, regexes_to_test, silent):
        rating = Rating()
        nr_of_regexes = len(regexes_to_test)
        nr_of_tasks = len(self.test_cases)
        if nr_of_regexes != nr_of_tasks:
            print_if_allowed(f"Wrong amount of regexes! (There are {nr_of_tasks} tasks, you gave {nr_of_regexes} solutions)", silent)
            raise ValueError
        for task_index, regex_to_test, test_cases in zip(range(nr_of_regexes), regexes_to_test, self.test_cases):
            task_results = TaskResults()
            task_results.regex = regex_to_test
            regex_was_attempted = regex_to_test.search_expr and (regex_to_test.search_expr != r"""Your_Regex_here""")
            if regex_was_attempted:
                task_index_str = f'# Task {task_index} ({regex_to_test.search_expr}) '
                print_if_allowed(task_index_str + '#' * (80 - len(task_index_str)), silent) # yeah, I'm a perfectionist, leave me alone
            silent_this_task = silent
            for test_case in test_cases:
                test_results = TestResults()
                task_results.attempted = regex_was_attempted
                if regex_was_attempted:
                    success = self.eval_regex(regex_to_test, test_case, silent_this_task)
                    if success:
                        test_results.passed = True
                    else:
                        silent_this_task = True
                task_results.test_results.append(test_results)
            rating.task_results.append(task_results)
        return rating

def test_my_regexes(regexes_to_test, silent=False):
    regex_tester = RegexTester()
    rating = regex_tester.test_regexes(regexes_to_test, silent)

    if rating.IsPassed():
        print_if_allowed('', silent)
        print_if_allowed('Yay, you did it! Bravo! If you liked this, you\'ll love that:\nhttps://regexcrossword.com/\nhttps://regex101.com/quiz\nhttps://v6p9d9t4.ssl.hwcdn.net/html/669329/RegExkcd/index.html', silent)
    else:
        rating.PrintOverview(silent)
    return rating

def print_if_allowed(str_to_print, silent):
    if not silent:
        print(str_to_print)

def print_without_newline_if_allowed(str_to_print, silent):
    if not silent:
        print(str_to_print, end='')
