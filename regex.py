from dataclasses import dataclass

@dataclass
class Regex:
  search_expr: str
  replace_expr: str = None
