import pathlib
import re
import shutil
import sys

from definitions import *

OVERWRITE_SOLUTIONS = False

if OVERWRITE_SOLUTIONS:
    my_input = input('Please confirm with \'y\' that you really want to force-overwrite everything! ')
    if my_input != 'y':
        print('Setting force back to false')
        OVERWRITE_SOLUTIONS = False

print(f'Generating template')
with open(FILE_MY_REGEXES) as my_regexes:
    my_regex_str = my_regexes.read()
    template_str = re.sub(pattern='search_expr=r""".*?"""', repl='search_expr=r"""' + UNATTEMPTED_REGEX + '"""',string=my_regex_str)
    template_str = re.sub(pattern='replace_expr=r""".*?"""', repl='replace_expr=r"""' + UNATTEMPTED_REPLACEMENT + '"""',string=template_str)
    task_nr = 0
    while True:
        before = template_str
        template_str = re.sub(pattern=TASK_NR_PLACEHOLDER, repl=str(task_nr), string=template_str, count=1)
        task_nr += 1
        if before == template_str:
            break

    with open(FILE_TEMPLATE, "w") as template_file:
        template_file.write(template_str)

for student in STUDENTS:
    print(f'Setting up student {student} #################')
    try:
        STUDENTS_PATH = sys.argv[1]
        student_path = pathlib.Path(STUDENTS_PATH) / pathlib.Path(student)
        student_path.mkdir(parents=False, exist_ok=True)

        file_monitor_me_not = student_path / FILE_MONITOR_ME_NOT
        if not file_monitor_me_not.exists():
            file_help_me = student_path / FILE_MONITOR_ME
            file_help_me.touch()
        else:
            print(f'Student wants to be left alone.')

        shutil.copy(FILE_NAME_REGEX, student_path / FILE_NAME_REGEX)
        shutil.copy(FILE_NAME_REGEX_TESTS, student_path / FILE_NAME_REGEX_TESTS)
        my_regexes_path = student_path / FILE_MY_REGEXES
        if not my_regexes_path.exists():
            shutil.copy(FILE_TEMPLATE, my_regexes_path)
        elif OVERWRITE_SOLUTIONS:
            print('Force-overwriting regexes of student {}')
            shutil.copy(FILE_TEMPLATE, my_regexes_path)
        else:
            print(f'Leaving student\'s solution as is.')
    except PermissionError:
        print(f'Could not setup student!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')

pathlib.Path(FILE_TEMPLATE).unlink()
