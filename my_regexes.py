from regex import Regex

my_regexes = [
    # Task __NR__ (yay in text)
    #
    # Your Regex should match if the word
    #
    # yay
    #
    # appears anywhere in the subject text.
    # This can be as a whole word, or as part of another word.
    #
    # Requirements & Special options:
    # - Matching a part of a word is ok
    Regex(search_expr=r"""yay"""),

    # Task __NR__ (only yay)
    #
    # This time we are much more restrictive. Your Regex should only match if the
    # subject text is exactly the word
    #
    # yay
    #
    # nothing pre- or postfixed!
    #
    # Requirements & Special options:
    # - Your Regex must match the whole subject string (with ^ and $)
    Regex(search_expr=r"""^yay$"""),

    # Task __NR__ (AE vs. BE)
    #
    # We would like to match any of the following words
    #
    # gray
    # grey
    #
    # but nothing else.
    #
    # Requirements & Special options:
    # - Your Regex must match the whole subject string (with ^ and $)
    Regex(search_expr=r"""^gr[ae]y$"""),

    # Task __NR__ (dada)
    #
    # Now we get absurd. We want to match any word
    #
    # gr_y
    #
    # where _ can be any symbol
    # (any letter, digit, special character, anything (except a newline))
    #
    # Requirements & Special options:
    # - Your Regex must match the whole subject string (with ^ and $)
    Regex(search_expr=r"""^gr.y$"""),

    # Task __NR__ (keywords)
    #
    # We want to search for some keywords. Search for any of the following words:
    #
    # if
    # else
    # while
    # do
    # for
    #
    # These words can appear in a larger text, so the subject string does not necessarily
    # consist of only these words. So no need to use ^ or $ (for now it's also ok if words
    # like fork are matched)
    #
    # Requirements & Special options:
    # - Matching a part of a word is ok
    Regex(search_expr=r"""if|else|while|do|for"""),

    # Task __NR__ (80 or 443)
    #
    # Check if the subject string only contains one of these words:
    #
    # http
    # https
    #
    # The challenge here is to keep the Regex short.
    #
    # Requirements & Special options:
    # - Your Regex must match the whole subject string (with ^ and $)
    Regex(search_expr=r"""^https?$"""),

    # Task __NR__ (10000)
    #
    # Check if the subject string is the number 1 with any number of 0s:
    #
    # 1: matches
    # 10: matches
    # 10000: matches
    # 20: does not match
    # 10020: does not match
    #
    # Requirements & Special options:
    # - Your Regex must match the whole subject string (with ^ and $)
    Regex(search_expr=r"""^10*$"""),

    # Task __NR__ (wohooo)
    #
    # Check if the subject string is the text "woh" + 1 or more "o":
    #
    # woho: matches
    # wohoooo: matches
    # wohooooooooooooooooooo: matches
    # woh: does not match
    # wohan: does not match
    #
    # Requirements & Special options:
    # - Your Regex must match the whole subject string (with ^ and $)
    Regex(search_expr=r"""^woho+$"""),

    # Task __NR__ (space, the final frontier)
    #
    # Some  people have a  jittery space key   thumb.
    # Write a Regex that finds two words with any number of spaces greater than one between them.
    # Assume that words consist only of letters (upper and lower case).
    # The two words to search for can appear in a larger text, so no need for ^ and $.
    #
    # Requirements & Special options:
    # - Matching a part of a word is ok
    Regex(search_expr=r"""[a-zA-Z]+ {2,}[a-zA-Z]+"""),

    # Task __NR__ (no vowels)
    #
    # Assume we don't like vowels. They're just not metal enough.
    # Write a Regex that matches any words that don't contain vowels (aeiouAEIOU).
    # The words should also not contain spaces, but they can contain numbers and
    # special characters.
    # klngn: matches
    # rgx: matches
    # rs232: matches
    # ç%&"*: matches
    # axy: does not match
    # CSA: does not match
    # x y: does not match
    #
    # Requirements & Special options:
    # - Your Regex must match the whole subject string (with ^ and $)
    Regex(search_expr=r"""^[^aeiouAEIOU ]+$"""),

    # Task __NR__ (stray {)
    #
    # The Google style guide asks that all opening braces are on the same line
    # as the if/while/... statement.
    # So we (normally) shouldn't find a line with just an opening brace in it.
    # Create a Regex that looks for lines that just contain an opening brace {
    # (which may be surrounded by whitespace).
    #
    # Requirements & Special options:
    # - Multi-line mode enabled
    # - Your Regex must match whole lines (with ^ and $)
    Regex(search_expr=r"""^\s*{\s*$"""),

    # Task __NR__ (079 het sie gseit)
    #
    # Help Lo & Leduc.
    # Check if the entered text is a phone number that starts with
    #
    # 079
    #
    # Assume that all valid phone numbers are formatted as
    #
    # 079 XXX XX XX
    #
    # where X are numbers between 0-9.
    # No international area-codes (+41) are used.
    # Make sure nothing else than the phone number was entered.
    #
    # Requirements & Special options:
    # - Your Regex must match the whole subject string (with ^ and $)
    Regex(search_expr=r"""^079 \d{3} \d{2} \d{2}$"""),

    # Task __NR__ (variable name)
    #
    # Make a regex to check if the entered subject text is a valid variable name.
    # For simplicity, assume that any variable name with one or more characters is
    # valid, if the characters are letters, numbers or underscores.
    #
    # Requirements & Special options:
    # - Your Regex must match the whole subject string (with ^ and $)
    Regex(search_expr=r"""^\w+$"""),

    # Task __NR__ (whole yay)
    #
    # Yet another "yay" scenario: We want to Match only whole words "yay", but they can appear
    # in a larger subject text.
    #
    # Requirements & Special options:
    # - Match only whole words
    Regex(search_expr=r"""\byay\b"""),

    # Task __NR__ (E-Mail)
    #
    # Search for valid E-Mail addresses. For simplicity,
    # a valid E-Mail is defined as:
    #
    # <one or more non-whitespace characters>@<one or more non-whitespace characters>.<2-6 letters>
    #
    # Make sure the subject string consists only of the E-Mail-Address, like in a web form.
    #
    # Requirements & Special options:
    # - Your Regex must match the whole subject string (with ^ and $)
    Regex(search_expr=r"""^\S+@\S+\.[a-z]{2,6}$"""),

    # Task __NR__ (duplicate words)
    #
    # Search for all duplicate consecutive words in a text. For example,
    # in the sample text
    #
    # this text has has several duplicates duplicates
    #
    # the matches should be:
    #
    # has
    # duplicates
    #
    # Words can only contain the letters a-z, and A-Z.
    # Between the words, there can be one or more whitespace.
    # Only whole words should be compared, for example word and wording are two
    # different words!
    #
    # Requirements & Special options:
    # - Match only whole words
    Regex(search_expr=r"""\b([a-zA-Z]+)\s+\1\b"""),

    # Task __NR__ (Google style guide)
    #
    # The Google style guide asks that all while statements have a space
    # between the keyword and the opening round brackets:
    #
    # while (foo) {
    #
    # This is incorrect:
    #
    # while(foo) {
    #
    #
    # Provide a Regex that finds all instances of such incorrect statements and
    # replaces them with the correct statement, containing the missing space.
    #
    # Insert your replacement text into the parameter "replace_expr", between the tripple quotes.
    #
    # Requirements & Special options:
    # - Multi-line mode enabled
    # - Your Regex must match whole lines (with ^ and $)
    Regex(search_expr=r"""^(while) \((.*)\){$""", replace_expr=r"""\1 (\2) {"""),

    # Task __NR__ (first names)
    #
    # Assume we have a list of names, always with first name, then last name:
    # Adrian Berger, Ben Zuger, Charlie Berger, Dominik Schaffner, Erik Berger,...
    #
    # Now we want to find all people with the last name "Berger". From those people
    # we want to match only the first name.
    #
    # Requirements & Special options:
    # - Match only whole words
    Regex(search_expr=r"""\b[a-zA-Z]+(?= Berger\b)"""),

    # Task __NR__ (q but not qu)
    #
    # Write a Regex that only matches words that start with a "q" that is not followed directly by a "u":
    #
    # q: matches
    # qu: does not match
    # qa: matches
    # qau: matches
    # aqa: does not match
    #
    # Requirements & Special options:
    # - Match only whole words
    Regex(search_expr=r"""\bq(?!u).*\b"""),

    # Task __NR__ (Michael's favourite)
    #
    # We want to find all the occurrences of two angle brackets <>
    # enclosing any amount of non-angle-bracket characters. So for example,
    # for the subject string
    #
    # foo <bar> zar <y_9>
    #
    # the matches are supposed to be
    #
    # <bar>
    # <y_9>
    #
    # Note that the angle brackets are also included in the matches for simplicity.
    #
    # Requirements & Special options:
    # - Matching a part of a word is ok
    Regex(search_expr=r"""<.*?>"""),

# Bonus tasks for the nerds ##############################################

    # Task __NR__ (bad cat)
    #
    # Assume for a moment, if you will, that we don't like cats. So we would like to write
    # a Regex that matches any whole word except "cat".
    #
    # cat: does not match
    # car: matches
    # rat: matches
    # catering: matches
    # netcat: matches
    #
    # Requirements & Special options:
    # - Match only whole words
    Regex(search_expr=r"""\b(?!cat$).+\b"""),

    # Task __NR__ (octals)
    #
    # We want to search for any octal numbers, that is, numbers that start
    # with 0. But we don't want to find the number 0 itself, as this might also be a
    # decimal number.
    # 018: matches
    # 0123: matches
    # 123: does not match
    # 0: does not match
    #
    # Requirements & Special options:
    # - Match only whole words
    Regex(search_expr=r"""\b0[0-7]+\b"""),

    # Task __NR__ (annoying pw checker)
    #
    # Make a (very badly designed and annoying) password checker that
    # checks the given input for five criteria:
    #
    # between 8 and 32 characters
    # at least 1 lowercase letter
    # at least 1 uppercase letter
    # at least 1 digit
    # at least one of those special characters: []{}:_$&
    #
    # Requirements & Special options:
    # - Your Regex must match the whole subject string (with ^ and $)
    Regex(search_expr=r"""^(?=.*[a-z]+)(?=.*[A-Z]+)(?=.*\d+)(?=.*[\[\]{}_$&]+).{8,32}$"""),

    # Task __NR__ (separators)
    #
    # Write a Regex that can match big integer numbers containing thousand separators
    # (Tausendertrennzeichen), for example
    #
    # 1'000'000
    #
    # We use the Swiss ' apostrophe as thousand separator.
    #
    # 1'000: matches
    # 1'000'000 matches
    # 100: matches
    # 0: matches
    # 1000: does not match
    # 10'00: does not match
    # 1000': does not match
    # '000: does not match
    #
    # Requirements & Special options:
    # - Your Regex must match the whole subject string (with ^ and $)
    Regex(search_expr=r"""^(0|(?:[1-9][0-9]{0,2}(?:'[0-9]{3})*))$"""),
]

if __name__ == '__main__':
    import regex_tests
    regex_tests.test_my_regexes(my_regexes)
