import importlib
import os
import pathlib
import sys

from definitions import *
from regex_tests import test_my_regexes

sys.path.append(PATH_CONTAINER_STUDENTS_WORK)
sys.path.append(PATH_CONTAINER_IGNORE_LIST)
from ignore_list import STUDENTS_TO_IGNORE

for student_to_ignore in STUDENTS_TO_IGNORE:
    try:
        STUDENTS.remove(student_to_ignore)
    except ValueError:
        print(f'There is no student {student_to_ignore}!')

ratings = []

for student in STUDENTS:
    try:
        student_path = pathlib.Path(PATH_CONTAINER_STUDENTS_WORK) / pathlib.Path(student)

        file_monitor_me_not = student_path / FILE_MONITOR_ME_NOT
        if not file_monitor_me_not.exists():
            student_regexes = importlib.import_module(student + '.my_regexes').my_regexes
            rating = test_my_regexes(regexes_to_test=student_regexes, silent=True)
            rating.student = student
            ratings.append(rating)
    except KeyboardInterrupt:
        print('Cancelled')
        raise
    except:
        pass

if ratings:
    ratings_best_first = sorted(ratings, key=lambda rating: rating.GetNrPassedTests(), reverse=True)
    os.system('clear')

    print(f"Leaderboard:")
    if ratings_best_first[0].GetNrPassedTests():
        for rating_best_first in ratings_best_first[:3]:
            if rating_best_first.GetNrPassedTests():
                print(f"{rating_best_first.student:20} passed {rating_best_first.GetNrPassedTests():3} tests and completed {rating_best_first.GetNrPassedTasks():2} tasks.")
    else:
        print(f'The battle has not yet commenced!')
else:
    print(f"No results!")
