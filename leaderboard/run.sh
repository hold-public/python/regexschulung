source ../path_definitions.sh

docker run \
-e TERM=xterm \
-v $PATH_HOST_STUDENTS_WORK_COPY:$PATH_CONTAINER_STUDENTS_WORK \
-v $PATH_HOST_IGNORE_LIST:$PATH_CONTAINER_IGNORE_LIST \
phygan/leaderboard
